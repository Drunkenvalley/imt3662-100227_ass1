package com.drunkenvalley.assignment1;

import android.app.ListActivity;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.SimpleCursorAdapter;
import android.app.LoaderManager;

public class MyListView extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {
	SimpleCursorAdapter MyAdapter;
	Uri Provider = Uri.parse("content://com.drunkenvalley.assignment1.SQLite/entry");

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String[] from={"content"};
		int[] to={android.R.id.text1};
		
		MyAdapter = new SimpleCursorAdapter(
				this, android.R.layout.simple_list_item_1, 	//Context, layout 
				null, from, to, 0); 						//Cursor, from, to, flags
		setListAdapter(MyAdapter);
        getLoaderManager().initLoader(0, null, this);
	}
	
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, Provider, null, "content NOT NULL", null, null);
    }
	
	public void onLoaderReset(Loader<Cursor> loader) {
        MyAdapter.swapCursor(null);
    }

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		// TODO Auto-generated method stub
		
	}

}

