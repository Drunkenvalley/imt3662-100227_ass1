package android.database.sqlite;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class SQLite extends ContentProvider {
	// To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public SQLite() {}
    SQLiteDatabase database;
	SQLiteHelper handler;
	long newRowId;
	String[] contentReturn;

    //Creating a helper subclass.
    public class SQLiteHelper extends SQLiteOpenHelper {
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "SQLite.db";

        private SQLiteHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE entry (entryid INTEGER PRIMARY KEY, content TEXT)");
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS entry");
            onCreate(db);
        }
        
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }
    
    public void open() {
		database = handler.getWritableDatabase();
	}
	
	public void close() {
		handler.close();
	}
	
	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri arg0, ContentValues content) {
		newRowId = database.insert("entry","content",content);
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		handler = new SQLiteHelper(getContext());
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cursor query(Uri arg0, String[] arg1, String arg2,
			String[] arg3, String arg4) {
		// TODO Auto-generated method stub
		Cursor c;
		c = database.rawQuery("SELECT content FROM entry SORTED BY entryid", null);
		return c;
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2,
			String[] arg3) {
		// TODO Auto-generated method stub
		return 0;
	}
}